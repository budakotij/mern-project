const mongoose = require('mongoose');

const postVideoSchema = new mongoose.Schema({
   userID:{
     type: mongoose.Types.ObjectId, required: true, ref: 'User'
   },
  VideoUrl: {
        type: String,
        default: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        required:true       
    },
  location:{
    name:{ type: String, required: true },
    lat: { type: String, required: true },
    lng: { type: String, required: true }
  },
  Caption:{
      type:String
  },
  likesCount:{
    type:Number,
    default:0
  },
  likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
  isTrip:{
      type:Boolean, required:true
  },
  TripID:{
      type: mongoose.Schema.Types.ObjectId,
        ref: 'Trip'
  },
  TimeStamp:{
      type:Number,
      required:true
  }
})


module.exports = mongoose.model('PostVideo', postVideoSchema);