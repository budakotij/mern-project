const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema({
  
target:{
     type: mongoose.Types.ObjectId, required: true, ref: 'User'
   
},
type:{
    type:Number, required:true
},
sender:{
     type: mongoose.Types.ObjectId, required: true, ref: 'User'

},
 photo:{
     type: mongoose.Types.ObjectId, required: true, ref: 'Photo'

 }, 
 video:{
     type: mongoose.Types.ObjectId, required: true, ref: 'Video'

 },
 trip:{
     type: mongoose.Types.ObjectId, required: true, ref: 'Trip'

 },
 timeStamp:{
     type:String, required:true
 },

})


module.exports = mongoose.model('Notifications',NotificationSchema);