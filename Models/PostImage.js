const mongoose = require('mongoose');

const postImageSchema = new mongoose.Schema({
   userID:{
     type: mongoose.Types.ObjectId, required: true, ref: 'User'
   },
  imageUrl: {
        type: String,
        default: "https://images.unsplash.com/photo-1514539079130-25950c84af65?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=749&q=80",
        required:true
    },
  location:{
    name:{ type: String, required: true },
    lat: { type: String, required: true },
    lng: { type: String, required: true }
  },
  Caption:{
      type:String
  },
  likesCount:{
    type:Number,
    default:0
  },
  likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
  isTrip:{
      type:Boolean, required:true
  },
  TripID:{
      type: mongoose.Schema.Types.ObjectId,
        ref: 'Trip'
  },
  TimeStamp:{
      type:Number,
      required:true
  }
})


module.exports = mongoose.model('PostImage', postImageSchema);