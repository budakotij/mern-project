const mongoose = require('mongoose');

const trip_schema = new mongoose.Schema({
    organizer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    participants: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    destination: {
        name:{ type: String, required: true },
        lat: { type: String, required: true },
        lng: { type: String, required: true }
    },
    start_place: {
        name:{ type: String, required: true },
        lat: { type: String, required: true },
        lng: { type: String, required: true }
    },
    start_date: {
        type: String,
        required: true
    },
    end_date: {
        type: String,
        required: true
    },
    transportation: {
        type: String,
        default: "N/A"
    },
    isSolo: {
        type: Boolean,
        required: true
    },
    estimate_cost: {
        type: Number,
        default: 0
    },
    highlights: [{
        type: String
    }],
    chat_url: {
        type: String,
        default: " "
    },
    total_likes: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model('Trip', trip_schema);