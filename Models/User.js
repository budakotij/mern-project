const mongoose = require('mongoose');

const user_schema = new mongoose.Schema({
    userName: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    profile_image: {
        type: String,
        default: "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cHJvZmlsZSUyMGltYWdlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
    background_image: {
        type: String,
        default: "https://images.unsplash.com/photo-1441974231531-c6227db76b6e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bmF0dXJlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
    bio_1: {
        type: String,
        default: ""
    },
    bio_2: {
        type: String,
        default: ""
    },
    bio_3: {
        type: String,
        default: ""
    },
    bio_4: {
        type: String,
        default: ""
    },
    interests: [{
        type: String
    }],
    phone_number: {
        type: Number
    },
    followers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    following: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    liked_post_photo: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PostImage'
    }],
    liked_post_video: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PostVideo'
    }]
})

module.exports = mongoose.model('User', user_schema);