const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors')


require('dotenv/config');

const connection_string = process.env.CONNECTION_STRING;
const api = process.env.API_URL;

const userRoute = require('./routes/userRoute');

//Middleware
app.use(morgan('tiny'));
app.use(express.json());
app.use(cors())
app.options('*', cors()) 

app.use(`${api}/users`, userRoute);




mongoose.connect(connection_string, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    console.log('connected to database')
}).catch(err => {
    console.log(err)
}).then(()=>{
    app.listen(3000, () => {
        console.log(`${api}/`);
        console.log('server has started successfully');
    })
})


